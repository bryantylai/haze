﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace HazeWP
{
    public class History
    {
        public Int32 API { get; set; }
        public String DateTime { get; set; }
        public String Difference { get; set; }
        public SolidColorBrush DiffColor { get; set; }
        public SolidColorBrush APIColor { get; set; }
    }
}
