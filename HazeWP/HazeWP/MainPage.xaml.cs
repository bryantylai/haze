﻿#define DEBUG_AGENT
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.Phone.Tasks;
using System.IO.IsolatedStorage;
using System.Windows.Threading;
using System.Collections.ObjectModel;
using System.Net.NetworkInformation;
using Microsoft.Phone.Scheduler;
using Microsoft.Phone.Shell;
using System.Xml.Linq;

namespace HazeWP
{
    public partial class MainPage : PhoneApplicationPage
    {
        Dictionary<String, String> locCollection = new Dictionary<String, String>();
        Dictionary<String, Int32> dataCollection = new Dictionary<String, Int32>();
        ObservableCollection<History> HistoryCollection = new ObservableCollection<History>();

        IsolatedStorageFile file = IsolatedStorageFile.GetUserStoreForApplication();

        Boolean infoGridShowing = false;
        Boolean historyGridShowing = false;
        Boolean settingsGridShowing = false;
        Boolean MenuShowing = false;

        // Constructor
        public MainPage()
        {
            InitializeComponent();

            locCollection.Add("Johor, Kota Tinggi", "kota_tinggi");
            locCollection.Add("Johor, Larkin Lama", "larkin_lama");
            locCollection.Add("Johor, Muar", "muar");
            locCollection.Add("Johor, Pasir Gudang", "pasir_gudang");
            locCollection.Add("Johor, Alor Setar", "alor_setar");
            locCollection.Add("Kedah, Bakar Arang, Sg. Petani", "bakar_arang");
            locCollection.Add("Kedah, Langkawi", "langkawi");
            locCollection.Add("Kelantan, Kota Bharu", "kota_bharu");
            locCollection.Add("Kelantan, Tanah Merah", "tanah_merah");
            locCollection.Add("Kuala Lumpur, Batu Muda", "batu_muda");
            locCollection.Add("Kuala Lumpur, Cheras", "cheras");
            locCollection.Add("Melaka, Bandaraya Melaka", "bandaraya_melaka");
            locCollection.Add("Melaka, Bukit Rambai", "bukit_rambai");
            locCollection.Add("Negeri Sembilan, Nilai", "nilai");
            locCollection.Add("Negeri Sembilan, Port Dickson", "port_dickson");
            locCollection.Add("Negeri Sembilan,, Seremban", "seremban");
            locCollection.Add("Pahang, Balok Baru, Kuantan", "balok_baru");
            locCollection.Add("Pahang, Indera Mahkota, Kuantan", "indera_mahkota");
            locCollection.Add("Pahang, Jerantut", "jerantut");
            locCollection.Add("Perak, Jalan Tasek, Ipoh", "jalan_tasek");
            locCollection.Add("Perak, Kg. Air Putih, Taiping", "air_putih");
            locCollection.Add("Perak, S K Jalan Pegoh, Ipoh", "sk_jalan_pegoh");
            locCollection.Add("Perak, Seri Manjung", "seri_manjung");
            locCollection.Add("Perak, Tanjung Malim", "tanjung_malim");
            locCollection.Add("Perlis, Kangar", "kangar");
            locCollection.Add("Pulau Pinang, Perai", "perai");
            locCollection.Add("Pulau Pinang, Seberang Jaya 2, Perai", "seberang_jaya_2");
            locCollection.Add("Pulau Pinang, USM", "usm");
            locCollection.Add("Sabah, Keningau", "keningau");
            locCollection.Add("Sabah, Kota Kinabalu", "kota_kinabalu");
            locCollection.Add("Sabah, Sandakan", "sandakan");
            locCollection.Add("Sabah, Tawau", "tawau");
            locCollection.Add("Sarawak, Bintulu", "bintulu");
            locCollection.Add("Sarawak, ILP Miri", "ilp_miri");
            locCollection.Add("Sarawak, Kapit", "kapit");
            locCollection.Add("Sarawak, Kuching", "kuching");
            locCollection.Add("Sarawak, Limbang", "limbang");
            locCollection.Add("Sarawak, Miri", "miri");
            locCollection.Add("Sarawak, Samarahan", "samarahan");
            locCollection.Add("Sarawak, Sarikei", "sarikei");
            locCollection.Add("Sarawak, Sibu", "sibu");
            locCollection.Add("Sarawak, Sri Aman", "sri_aman");
            locCollection.Add("Selangor, Banting", "banting");
            locCollection.Add("Selangor, Kuala Selangor", "kuala_selangor");
            locCollection.Add("Selangor, Pelabuhan Klang", "pelabuhan_kelang");
            locCollection.Add("Selangor, (Puchong) Petaling Jaya", "petaling_jaya");
            locCollection.Add("Selangor, Shah Alam", "shah_alam");
            locCollection.Add("Terengganu, Kemaman", "kemaman");
            locCollection.Add("Terengganu, Kuala Terengganu", "kuala_terengganu");
            locCollection.Add("Wilayah Persekutuan, Labuan", "labuan");
            locCollection.Add("Wilayah Persekutuan, Putrajaya", "putrajaya");

            List<String> keys = new List<String>(locCollection.Keys);
            LocationListPicker.ItemsSource = keys;

            try
            {
                using (StreamReader readFile = new StreamReader(new IsolatedStorageFileStream("default.txt", FileMode.Open, FileAccess.Read, file)))
                {
                    LocationListPicker.SelectedItem = readFile.ReadLine();
                }
            }
            catch
            {
                int random = new Random().Next(keys.Count);
                LocationListPicker.SelectedIndex = random;
            }

            LocationListPicker.SelectionChanged += LocationListPicker_SelectionChanged;

            versionText.Text = "Version " + XDocument.Load("WMAppManifest.xml").Root.Element("App").Attribute("Version").Value;

            emailButton.Label = "Email";
            emailButton.ImageUri = new Uri("Assets/Email.png", UriKind.RelativeOrAbsolute);

            reviewButton.Label = "Rate & Review";
            reviewButton.ImageUri = new Uri("Assets/Review.png", UriKind.RelativeOrAbsolute);
        }

        /// <summary>
        /// Get results from web
        /// </summary>
        /// <param name="uri">website uri</param>
        private void GetResults(String uri)
        {
            if (NetworkInterface.GetIsNetworkAvailable())
            {
                HttpWebRequest req = HttpWebRequest.CreateHttp(uri);
                req.BeginGetResponse(new AsyncCallback(ReadWebRequestCallBack), req);
            }
            else
            {
                MessageBox.Show("Please ensure you are connected to internet to view API.");
            }
        }

        private void ReadWebRequestCallBack(IAsyncResult result)
        {
            try
            {
                HttpWebRequest req = (HttpWebRequest)result.AsyncState;
                HttpWebResponse res = (HttpWebResponse)req.EndGetResponse(result);

                using (StreamReader httpwebStreamReader = new StreamReader(res.GetResponseStream()))
                {
                    String results = httpwebStreamReader.ReadToEnd();

                    MatchCollection psinow = Regex.Matches(results, "<div class=\"psinow\">.*?</div>", RegexOptions.Singleline);
                    MatchCollection psinowdate = Regex.Matches(results, "<div class=\"psinowdate\">.*?</div>", RegexOptions.Singleline);
                    MatchCollection psiolddate = Regex.Matches(results, "<div class=\"psiolddate\".*?</div>", RegexOptions.Singleline);
                    MatchCollection psihistory = Regex.Matches(results, "<div class=\"eight columns mobile-four\">.*?<div class=\"psiolddate\" style=\"\">.*?</div>", RegexOptions.Singleline);

                    // Process results on another thread
                    Dispatcher.BeginInvoke(() => ProcessResults(psinow[0], psinowdate[0], psiolddate[1]));
                    Dispatcher.BeginInvoke(() => ProcessHistory(psihistory));
                }
                res.Close();
            }
            catch
            {

            }
        }

        private void ProcessHistory(MatchCollection psihistory)
        {
            foreach (Match historyMatch in psihistory)
            {
                MatchCollection apiCollection = Regex.Matches(historyMatch.Value, "<div class=\"psiold\".*?</div>", RegexOptions.Singleline);
                MatchCollection dateCollection = Regex.Matches(historyMatch.Value, "<div class=\"psiolddate\".*?</div>", RegexOptions.Singleline);

                History h = new History();
                h.API = Convert.ToInt32(Regex.Replace(apiCollection[0].Value, @"<[^<]*>", String.Empty));
                h.DateTime = Regex.Replace(dateCollection[0].Value, @"<[^<]*>", String.Empty) + " " + Regex.Replace(dateCollection[1].Value, @"<[^<]*>", String.Empty);
                HistoryCollection.Add(h);

                if (h.API <= 50)
                {
                    h.APIColor = new SolidColorBrush(Color.FromArgb(255, 102, 255, 153));
                }
                else if (h.API <= 100)
                {
                    h.APIColor = new SolidColorBrush(Color.FromArgb(255, 255, 255, 153));
                }
                else if (h.API <= 150)
                {
                    h.APIColor = new SolidColorBrush(Color.FromArgb(255, 255, 153, 102));
                }
                else if (h.API <= 200)
                {
                    h.APIColor = new SolidColorBrush(Color.FromArgb(255, 255, 153, 102));
                }
                else if (h.API <= 250)
                {
                    h.APIColor = new SolidColorBrush(Color.FromArgb(255, 204, 0, 51));
                }
                else if (h.API <= 300)
                {
                    h.APIColor = new SolidColorBrush(Color.FromArgb(255, 204, 0, 51));
                }
                else
                {
                    h.APIColor = new SolidColorBrush(Color.FromArgb(255, 160, 0, 0));
                }
            }

            for (int i = 0; i < HistoryCollection.Count - 1; i++)
            {
                int diff = HistoryCollection[i].API - HistoryCollection[i + 1].API;

                if (diff <= 0)
                {
                    HistoryCollection[i].DiffColor = new SolidColorBrush(Color.FromArgb(255, 102, 255, 153));
                    HistoryCollection[i].Difference = "( " + diff + " )";
                }
                else if (diff > 0)
                {
                    HistoryCollection[i].DiffColor = new SolidColorBrush(Color.FromArgb(255, 204, 0, 51));
                    HistoryCollection[i].Difference = "( +" + diff + " )";
                }
            }

            HistoryCollection.RemoveAt(HistoryCollection.Count - 1);

            historyList.ItemsSource = HistoryCollection;
        }

        private void ProcessResults(Match m1, Match m2, Match m3)
        {
            String psinow = Regex.Replace(m1.Value, @"<[^<]*>", String.Empty);
            String psinowdate = Regex.Replace(m2.Value, @"<[^<]*>", String.Empty);
            String psiolddate = Regex.Replace(m3.Value, @"<[^<]*>", String.Empty);

            apiText.Text = psinow;
            timeText.Text = "Last updated " + psinowdate + psiolddate;

            int api = Convert.ToInt32(psinow);

            if (api <= 50)
            {
                apiText.Foreground = new SolidColorBrush(Color.FromArgb(255, 102, 255, 153));
                descText.Foreground = new SolidColorBrush(Color.FromArgb(255, 102, 255, 153));
                descText.Text = "No health implications";
            }
            else if (api <= 100)
            {
                apiText.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 255, 153));
                descText.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 255, 153));
                descText.Text = "No health implications";
            }
            else if (api <= 150)
            {
                apiText.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 153, 102));
                descText.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 153, 102));
                descText.Text = "Slight irritations may occur, individuals with breathing problems should reduce outdoor activities";
            }
            else if (api <= 200)
            {
                apiText.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 153, 102));
                descText.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 153, 102));
                descText.Text = "Slight irritations may occur, individuals with breathing problems should reduce outdoor activities";
            }
            else if (api <= 250)
            {
                apiText.Foreground = new SolidColorBrush(Color.FromArgb(255, 204, 0, 51));
                descText.Foreground = new SolidColorBrush(Color.FromArgb(255, 204, 0, 51));
                descText.Text = "Healthy people will be noticeably affected. People with breathing or heart problems will experience reduced endurance in activities. These individuals and elders should remain indoors and restrict activities.";
            }
            else if (api <= 300)
            {
                apiText.Foreground = new SolidColorBrush(Color.FromArgb(255, 204, 0, 51));
                descText.Foreground = new SolidColorBrush(Color.FromArgb(255, 204, 0, 51));
                descText.Text = "Healthy people will be noticeably affected. People with breathing or heart problems will experience reduced endurance in activities. These individuals and elders should remain indoors and restrict activities.";
            }
            else
            {
                apiText.Foreground = new SolidColorBrush(Color.FromArgb(255, 160, 0, 0));
                descText.Foreground = new SolidColorBrush(Color.FromArgb(255, 160, 0, 0));
                descText.Text = "Healthy people will experience reduced endurance in activities. There may be strong irritations and symptoms and may trigger other illnesses. Elders and the sick should remain indoors and avoid exercise. Healthy individuals should avoid out door activities.";
            }

            //if (liveTileSwitch.IsChecked == true)
            UpdateTile(api, psinowdate);
            dataBar.Visibility = Visibility.Collapsed;
        }

        private void UpdateTile(int api, String psinowdate)
        {
            // get application tile
            ShellTile tile = ShellTile.ActiveTiles.First();
            if (null != tile)
            {
                // creata a new data for tile
                StandardTileData data = new StandardTileData();
                // tile foreground data
                data.Title = "MY Haze";
                data.BackgroundImage = new Uri("/Background.png", UriKind.Relative);
                // to make tile flip add data to background also
                data.BackTitle = "MY Haze";
                data.BackContent = "Current API : " + api + "\nLast update " + psinowdate;
                // update tile
                tile.Update(data);

                StartPeriodicAgent();
            }
        }

        private void LocationListPicker_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            using (StreamWriter writeFile = new StreamWriter(new IsolatedStorageFileStream("default.txt", FileMode.Create, FileAccess.Write, file)))
            {
                writeFile.WriteLine((String)LocationListPicker.SelectedItem);
                writeFile.Close();
            }

            Reset();
        }

        private void Reset()
        {
            timeText.Text = "";
            apiText.Text = "";
            descText.Text = "";
            HistoryCollection.Clear();

            dataBar.Visibility = Visibility.Visible;

            String locationID;
            String location = (String)LocationListPicker.SelectedItem;
            locCollection.TryGetValue(location, out locationID);
            GetResults("http://apps.evozi.com/myapi/?loc=" + locationID);
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            base.OnBackKeyPress(e);

            if (historyGridShowing)
            {
                ReverseHistoryGridAnimation.Begin();

                e.Cancel = true;
            }

            if (infoGridShowing)
            {
                ReverseInfoGridAnimation.Begin();

                e.Cancel = true;
            }

            if (settingsGridShowing)
            {
                ReverseSettingsGridAnimation.Begin();

                e.Cancel = true;
            }
        }

        #region AppBar Functions

        private void historyAppBarButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (historyAppBarButton.Opacity != 0)
            {
                ReverseAllAnimations();

                HistoryGridAnimation.Begin();

            }
        }

        #region HistoryGridAnimations

        void ReverseHistoryGridAnimation_Completed(object sender, EventArgs e)
        {
            historyGridShowing = false;
        }

        void HistoryGridAnimation_Completed(object sender, EventArgs e)
        {
            historyGridShowing = true;
        }

        #endregion

        private void infoAppBarButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (infoAppBarButton.Opacity != 0)
            {
                ReverseAllAnimations();

                InfoGridAnimation.Begin();

            }
        }

        #region InfoGridAnimations

        void ReverseInfoGridAnimation_Completed(object sender, EventArgs e)
        {
            infoGridShowing = false;
        }

        void InfoGridAnimation_Completed(object sender, EventArgs e)
        {
            infoGridShowing = true;
        }

        #endregion

        private void shareAppBarButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (shareAppBarButton.Opacity != 0)
            {
                ShareStatusTask task = new ShareStatusTask();
                task.Status = "Current API at " + (String)LocationListPicker.SelectedItem + " is " + apiText.Text + ". " + timeText.Text + " via MY Haze #haze #myhaze http://aka.ms/myhaze";
                task.Show();
            }
        }

        private void refreshAppBarButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (refreshAppBarButton.Opacity != 0)
            {
                Reset();
            }
        }

        private void settingsAppBarButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (settingsAppBarButton.Opacity != 0)
            {
                ReverseAllAnimations();

                SettingsGridAnimation.Begin();

            }
        }

        private void ReverseAllAnimations()
        {
            if (historyGridShowing)
                ReverseHistoryGridAnimation.Begin();
            if (infoGridShowing)
                ReverseInfoGridAnimation.Begin();
            if (settingsGridShowing)
                ReverseSettingsGridAnimation.Begin();
        }

        #region SettingsGridAnimations

        void ReverseSettingsGridAnimation_Completed(object sender, EventArgs e)
        {
            settingsGridShowing = false;
        }

        void SettingsGridAnimation_Completed(object sender, EventArgs e)
        {
            settingsGridShowing = true;
        }

        #endregion

        #endregion

        #region CustomisedAppBar/Menu

        private void MenuAnimation_Completed(object sender, EventArgs e)
        {
            MenuShowing = true;
        }

        private void ReverseMenuAnimation_Completed(object sender, EventArgs e)
        {
            MenuShowing = false;
        }

        private void appBar_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (MenuShowing)
                ReverseMenuAnimation.Begin();
            else
                MenuAnimation.Begin();
        }

        #endregion

        private void emailButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            EmailComposeTask task = new EmailComposeTask();
            task.Subject = "Regarding MY Haze";
            task.To = "bryanlai@mystudentpartners.com";
            task.Show();
        }

        private void reviewButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            MarketplaceReviewTask task = new MarketplaceReviewTask();
            task.Show();
        }

        // Scheduled Task Agent
        private string periodicTaskName = "MYHazeLiveTile";
        private PeriodicTask periodicTask;

        private void StartPeriodicAgent()
        {
            // is old task running, remove it
            periodicTask = ScheduledActionService.Find(periodicTaskName) as PeriodicTask;
            if (periodicTask != null)
            {
                try
                {
                    ScheduledActionService.Remove(periodicTaskName);
                }
                catch (Exception)
                {
                }
            }
            // create a new task
            periodicTask = new PeriodicTask(periodicTaskName);
            // load description from localized strings
            periodicTask.Description = "Keep MY Haze to continue running in the background update the latest Air Pollution Index (API) and display on to the Live Tile";
            // set expiration days
            periodicTask.ExpirationTime = DateTime.Now.AddDays(14);
            try
            {
                // add thas to scheduled action service
                ScheduledActionService.Add(periodicTask);
                // debug, so run in every 30 secs
#if(DEBUG_AGENT)
                ScheduledActionService.LaunchForTest(periodicTaskName, TimeSpan.FromMinutes(15));
                System.Diagnostics.Debug.WriteLine("Periodic task is started: " + periodicTaskName);
#endif

            }
            catch (InvalidOperationException exception)
            {
                if (exception.Message.Contains("BNS Error: The action is disabled"))
                {
                    // load error text from localized strings
                    MessageBox.Show("Background task for MY Haze has been disabled. Please enable the background task for MY Haze/");
                }
                if (exception.Message.Contains("BNS Error: The maximum number of ScheduledActions of this type have already been added."))
                {
                    // No user action required. The system prompts the user when the hard limit of periodic tasks has been reached.
                }
            }
            catch (SchedulerServiceException)
            {
                // No user action required.
            }
        }

        #region LiveTile
        /*
        private void liveTileSwitch_Checked(object sender, RoutedEventArgs e)
        {
            Reset();
        }

        private void liveTileSwitch_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                // is old task running, remove it
                periodicTask = ScheduledActionService.Find(periodicTaskName) as PeriodicTask;
                if (periodicTask != null)
                {
                    ScheduledActionService.Remove(periodicTaskName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        */
        #endregion

        /*
         *
        private void defaultMenuItem_Click(object sender, EventArgs e)
        {
            using (StreamWriter writeFile = new StreamWriter(new IsolatedStorageFileStream("default.txt", FileMode.Create, FileAccess.Write, file)))
            {
                writeFile.WriteLine((String)LocationListPicker.SelectedItem);
                writeFile.Close();
                MessageBox.Show("You have set \'" + (String)LocationListPicker.SelectedItem + "\' as default location");
            }

            int api = 0;

            if (api <= 50)
            {
                apiText.Foreground = new SolidColorBrush(Color.FromArgb(255, 102, 255, 153));
                descText.Foreground = new SolidColorBrush(Color.FromArgb(255, 102, 255, 153));
                descText.Text = "No health implications";
            }
            else if (api <= 100)
            {
                apiText.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 255, 153));
                descText.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 255, 153));
                descText.Text = "No health implications";
            }
            else if (api <= 150)
            {
                apiText.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 153, 102));
                descText.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 153, 102));
                descText.Text = "Slight irritations may occur, individuals with breathing problems should reduce outdoor activities";
            }
            else if (api <= 200)
            {
                apiText.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 153, 102));
                descText.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 153, 102));
                descText.Text = "Slight irritations may occur, individuals with breathing problems should reduce outdoor activities";
            }
            else if (api <= 250)
            {
                apiText.Foreground = new SolidColorBrush(Color.FromArgb(255, 204, 0, 51));
                descText.Foreground = new SolidColorBrush(Color.FromArgb(255, 204, 0, 51));
                descText.Text = "Healthy people will be noticeably affected. People with breathing or heart problems will experience reduced endurance in activities. These individuals and elders should remain indoors and restrict activities.";
            }
            else if (api <= 300)
            {
                apiText.Foreground = new SolidColorBrush(Color.FromArgb(255, 204, 0, 51));
                descText.Foreground = new SolidColorBrush(Color.FromArgb(255, 204, 0, 51));
                descText.Text = "Healthy people will be noticeably affected. People with breathing or heart problems will experience reduced endurance in activities. These individuals and elders should remain indoors and restrict activities.";
            }
            else
            {
                apiText.Foreground = new SolidColorBrush(Color.FromArgb(255, 160, 0, 0));
                descText.Foreground = new SolidColorBrush(Color.FromArgb(255, 160, 0, 0));
                descText.Text = "Healthy people will experience reduced endurance in activities. There may be strong irritations and symptoms and may trigger other illnesses. Elders and the sick should remain indoors and avoid exercise. Healthy individuals should avoid out door activities.";
            }
        }
         * 
         */
    }
}