﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using Windows.ApplicationModel.DataTransfer;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.System;
using Windows.UI;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace HazeWin8
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class MainPage : HazeWin8.Common.LayoutAwarePage
    {
        public ObservableCollection<State> StateCollection = new ObservableCollection<State>();

        public MainPage()
        {
            this.InitializeComponent();

            GetResults();
        }

        private async void GetResults()
        {

            State Johor = new State() { StateName = "Johor", Cities = new ObservableCollection<City>() };
            Johor.Cities.Add(new City() { ID = "kota_tinggi", Location = "Kota Tinggi", State = ", Johor" });
            Johor.Cities.Add(new City() { ID = "larkin_lama", Location = "Larkin Lama", State = ", Johor" });
            Johor.Cities.Add(new City() { ID = "muar", Location = "Muar", State = ", Johor" });
            Johor.Cities.Add(new City() { ID = "pasir_gudang", Location = "Pasir Gudang", State = ", Johor" });

            State Kedah = new State() { StateName = "Kedah", Cities = new ObservableCollection<City>() };
            Kedah.Cities.Add(new City() { ID = "alor_setar", Location = "Alor Setar", State = ", Kedah" });
            Kedah.Cities.Add(new City() { ID = "bakar_arang", Location = "Bakar Arang, Sg. Petani", State = ", Kedah" });
            Kedah.Cities.Add(new City() { ID = "langkawi", Location = "Langkawi", State = ", Kedah" });

            State Kelantan = new State() { StateName = "Kelantan", Cities = new ObservableCollection<City>() };
            Kelantan.Cities.Add(new City() { ID = "kota_bharu", Location = "Kota Bharu", State = ", Kelantan" });
            Kelantan.Cities.Add(new City() { ID = "tanah_merah", Location = "Tanah Merah", State = ", Kelantan" });

            State Melaka = new State() { StateName = "Melaka", Cities = new ObservableCollection<City>() };
            Melaka.Cities.Add(new City() { ID = "bandaraya_melaka", Location = "Bandaraya Melaka", State = ", Melaka" });
            Melaka.Cities.Add(new City() { ID = "bukit_rambai", Location = "Bukit Rambai", State = ", Melaka" });

            State NSembilan = new State() { StateName = "Negeri Sembilan", Cities = new ObservableCollection<City>() };
            NSembilan.Cities.Add(new City() { ID = "nilai", Location = "Nilai", State = ", Negeri Sembilan" });
            NSembilan.Cities.Add(new City() { ID = "port_dickson", Location = "Port Dickson", State = ", Negeri Sembilan" });
            NSembilan.Cities.Add(new City() { ID = "seremban", Location = "Seremban", State = ", Negeri Sembilan" });

            State Pahang = new State() { StateName = "Pahang", Cities = new ObservableCollection<City>() };
            Pahang.Cities.Add(new City() { ID = "balok_baru", Location = "Balok Baru, Kuantan", State = ", Pahang" });
            Pahang.Cities.Add(new City() { ID = "indera_mahkota", Location = "Indera Mahkota, Kuantan", State = ", Pahang" });
            Pahang.Cities.Add(new City() { ID = "jerantut", Location = "Jerantut", State = ", Pahang" });

            State Perak = new State() { StateName = "Perak", Cities = new ObservableCollection<City>() };
            Perak.Cities.Add(new City() { ID = "jalan_tasek", Location = "Jalan Tasek, Ipoh", State = ", Perak" });
            Perak.Cities.Add(new City() { ID = "air_putih", Location = "Kg. Air Putih, Taiping", State = ", Perak" });
            Perak.Cities.Add(new City() { ID = "sk_jalan_pegoh", Location = "S K Jalan Pegoh, Ipoh", State = ", Perak" });
            Perak.Cities.Add(new City() { ID = "seri_manjung", Location = "Seri Manjung", State = ", Perak" });
            Perak.Cities.Add(new City() { ID = "tanjung_malim", Location = "Tanjung Malim", State = ", Perak" });

            State Perlis = new State() { StateName = "Perlis", Cities = new ObservableCollection<City>() };
            Perlis.Cities.Add(new City() { ID = "kangar", Location = "Kangar", State = ", Perlis" });

            State Pinang = new State() { StateName = "Pulau Pinang", Cities = new ObservableCollection<City>() };
            Pinang.Cities.Add(new City() { ID = "perai", Location = "Perai", State = ", Pinang" });
            Pinang.Cities.Add(new City() { ID = "seberang_jaya_2", Location = "Seberang Jaya 2", State = ", Pinang" });
            Pinang.Cities.Add(new City() { ID = "usm", Location = "USM", State = ", Pinang" });

            State Sabah = new State() { StateName = "Sabah", Cities = new ObservableCollection<City>() };
            Sabah.Cities.Add(new City() { ID = "keningau", Location = "Keningau", State = ", Sabah" });
            Sabah.Cities.Add(new City() { ID = "kota_kinabalu", Location = "Kota Kinabalu", State = ", Sabah" });
            Sabah.Cities.Add(new City() { ID = "sandakan", Location = "Sandakan", State = ", Sabah" });
            Sabah.Cities.Add(new City() { ID = "tawau", Location = "Tawau", State = ", Sabah" });

            State Sarawak = new State() { StateName = "Sarawak", Cities = new ObservableCollection<City>() };
            Sarawak.Cities.Add(new City() { ID = "bintulu", Location = "Bintulu", State = ", Sarawak" });
            Sarawak.Cities.Add(new City() { ID = "ilp_miri", Location = "ILP Miri", State = ", Sarawak" });
            Sarawak.Cities.Add(new City() { ID = "kapit", Location = "Kapit", State = ", Sarawak" });
            Sarawak.Cities.Add(new City() { ID = "kuching", Location = "Kuching", State = ", Sarawak" });
            Sarawak.Cities.Add(new City() { ID = "limbang", Location = "Limbang", State = ", Sarawak" });
            Sarawak.Cities.Add(new City() { ID = "miri", Location = "Miri", State = ", Sarawak" });
            Sarawak.Cities.Add(new City() { ID = "samarahan", Location = "Samarahan", State = ", Sarawak" });
            Sarawak.Cities.Add(new City() { ID = "sarikei", Location = "Sarikei", State = ", Sarawak" });
            Sarawak.Cities.Add(new City() { ID = "sibu", Location = "Sibu", State = ", Sarawak" });
            Sarawak.Cities.Add(new City() { ID = "sri_aman", Location = "Sri Aman", State = ", Sarawak" });

            State Selangor = new State() { StateName = "Selangor", Cities = new ObservableCollection<City>() };
            Selangor.Cities.Add(new City() { ID = "banting", Location = "Banting", State = ", Selangor" });
            Selangor.Cities.Add(new City() { ID = "kuala_selangor", Location = "Kuala Selangor", State = ", Selangor" });
            Selangor.Cities.Add(new City() { ID = "pelabuhan_klang", Location = "Pelabuhan Klang", State = ", Selangor" });
            Selangor.Cities.Add(new City() { ID = "petaling_jaya", Location = "(Puchong) Petaling Jaya", State = ", Selangor" });
            Selangor.Cities.Add(new City() { ID = "shah_alam", Location = "Shah Alam", State = ", Selangor" });

            State Terengganu = new State() { StateName = "Terengganu", Cities = new ObservableCollection<City>() };
            Terengganu.Cities.Add(new City() { ID = "kemaman", Location = "Kemaman", State = ", Terengganu" });
            Terengganu.Cities.Add(new City() { ID = "kuala_terengganu", Location = "Kuala Terengganu", State = ", Terengganu" });

            State Wilayah = new State() { StateName = "Wilayah Persekutuan", Cities = new ObservableCollection<City>() };
            Wilayah.Cities.Add(new City() { ID = "batu_muda", Location = "Batu Muda, Kuala Lumpur", State = "" });
            Wilayah.Cities.Add(new City() { ID = "cheras", Location = "Cheras, Kuala Lumpur", State = "" });
            Wilayah.Cities.Add(new City() { ID = "labuan", Location = "Labuan", State = ", Wilayah Persekutuan" });
            Wilayah.Cities.Add(new City() { ID = "putrajaya", Location = "Putrajaya", State = ", Wilayah Persekutuan" });

            StateCollection.Add(Johor);
            StateCollection.Add(Kedah);
            StateCollection.Add(Kelantan);
            StateCollection.Add(Melaka);
            StateCollection.Add(NSembilan);
            StateCollection.Add(Pahang);
            StateCollection.Add(Perak);
            StateCollection.Add(Perlis);
            StateCollection.Add(Pinang);
            StateCollection.Add(Sabah);
            StateCollection.Add(Sarawak);
            StateCollection.Add(Selangor);
            StateCollection.Add(Terengganu);
            StateCollection.Add(Wilayah);

            Boolean success = true;
            try
            {
                foreach (State s in StateCollection)
                {
                    foreach (City c in s.Cities)
                    {
                        HttpClient httpClient = new HttpClient();
                        Uri resource;

                        Uri.TryCreate("http://apps.evozi.com/myapi/?loc=" + c.ID, UriKind.RelativeOrAbsolute, out resource);

                        HttpResponseMessage response = await httpClient.GetAsync(resource);
                        String results = await response.Content.ReadAsStringAsync();

                        MatchCollection psinow = Regex.Matches(results, "<div class=\"psinow\">.*?</div>", RegexOptions.Singleline);
                        MatchCollection psinowdate = Regex.Matches(results, "<div class=\"psinowdate\">.*?</div>", RegexOptions.Singleline);
                        MatchCollection psiolddate = Regex.Matches(results, "<div class=\"psiolddate\".*?</div>", RegexOptions.Singleline);
                        //MatchCollection psihistory = Regex.Matches(results, "<div class=\"eight columns mobile-four\">.*?<div class=\"psiolddate\" style=\"\">.*?</div>", RegexOptions.Singleline);

                        ProcessResults(c, psinow[0], psinowdate[0], psiolddate[1]);
                    }
                }

                CollectionViewSource collections = new CollectionViewSource();
                collections.IsSourceGrouped = true;
                collections.Source = StateCollection;
                collections.ItemsPath = new PropertyPath("Cities");

                itemsGrid.ItemsSource = collections.View;

                itemsGrid.SelectedItem = null;
            }
            catch (HttpRequestException)
            {
                success = false;
            }

            progressRing.IsActive = false;

            if (!success)
                await new MessageDialog("There is an error in retrieving Air Pollution Index. Please ensure you have a stable internet connection and refresh MY Haze").ShowAsync();
        }

        private void ProcessResults(City c, Match m1, Match m2, Match m3)
        {
            String psinow = Regex.Replace(m1.Value, @"<[^<]*>", String.Empty);
            String psinowdate = Regex.Replace(m2.Value, @"<[^<]*>", String.Empty);
            String psiolddate = Regex.Replace(m3.Value, @"<[^<]*>", String.Empty);

            c.API = Convert.ToInt32(psinow);
            c.Updated = "Last updated " + psinowdate + psiolddate;

            int api = c.API;
            if (api <= 50)
            {
                c.Seriousness = new SolidColorBrush(Color.FromArgb(255, 102, 255, 153));
                //descText.Foreground = new SolidColorBrush(Color.FromArgb(255, 102, 255, 153));
                //descText.Text = "No health implications";
            }
            else if (api <= 100)
            {
                c.Seriousness = new SolidColorBrush(Color.FromArgb(255, 255, 255, 153));
                //descText.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 255, 153));
                //descText.Text = "No health implications";
            }
            else if (api <= 150)
            {
                c.Seriousness = new SolidColorBrush(Color.FromArgb(255, 255, 153, 102));
                //descText.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 153, 102));
                //descText.Text = "Slight irritations may occur, individuals with breathing problems should reduce outdoor activities";
            }
            else if (api <= 200)
            {
                c.Seriousness = new SolidColorBrush(Color.FromArgb(255, 255, 153, 102));
                //descText.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 153, 102));
                //descText.Text = "Slight irritations may occur, individuals with breathing problems should reduce outdoor activities";
            }
            else if (api <= 250)
            {
                c.Seriousness = new SolidColorBrush(Color.FromArgb(255, 204, 0, 51));
                //descText.Foreground = new SolidColorBrush(Color.FromArgb(255, 204, 0, 51));
                //descText.Text = "Healthy people will be noticeably affected. People with breathing or heart problems will experience reduced endurance in activities. These individuals and elders should remain indoors and restrict activities.";
            }
            else if (api <= 300)
            {
                c.Seriousness = new SolidColorBrush(Color.FromArgb(255, 204, 0, 51));
                //descText.Foreground = new SolidColorBrush(Color.FromArgb(255, 204, 0, 51));
                //descText.Text = "Healthy people will be noticeably affected. People with breathing or heart problems will experience reduced endurance in activities. These individuals and elders should remain indoors and restrict activities.";
            }
            else
            {
                c.Seriousness = new SolidColorBrush(Color.FromArgb(255, 160, 0, 0));
                //descText.Foreground = new SolidColorBrush(Color.FromArgb(255, 160, 0, 0));
                //descText.Text = "Healthy people will experience reduced endurance in activities. There may be strong irritations and symptoms and may trigger other illnesses. Elders and the sick should remain indoors and avoid exercise. Healthy individuals should avoid out door activities.";
            }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
            // Register for DataRequested events
            DataTransferManager.GetForCurrentView().DataRequested += OnDataRequested;
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="pageState">An empty dictionary to be populated with serializable state.</param>
        protected override void SaveState(Dictionary<String, Object> pageState)
        {
            // Deregister the DataRequested event handler
            DataTransferManager.GetForCurrentView().DataRequested -= OnDataRequested;
        }

        async void OnDataRequested(DataTransferManager sender, DataRequestedEventArgs args)
        {
            try
            {
                var request = args.Request;

                City c = (City)itemsGrid.SelectedItem;

                request.Data.Properties.Title = "MY Haze";
                request.Data.SetText("Current API at " + c.Location + c.State + " is " + c.API + ". " + c.Updated + " via MY Haze #haze #myhaze");
                return;
            }
            catch (Exception)
            {
            }

            await new MessageDialog("You have not selected any API to share with.").ShowAsync();
        }

        private void itemsGrid_ItemClick(object sender, ItemClickEventArgs e)
        {
            City c = (City)e.ClickedItem;
            itemsGrid.SelectedItem = c;
        }

        private void refreshAppBarButton_Click(object sender, RoutedEventArgs e)
        {
            if (progressRing.IsActive)
                return;

            StateCollection.Clear();
            itemsGrid.ItemsSource = null;

            progressRing.IsActive = true;

            GetResults();
        }

        private async void phoneAppBarButton_Click(object sender, RoutedEventArgs e)
        {
            Uri uri = new Uri("http://aka.ms/myhaze", UriKind.Absolute);
            await Launcher.LaunchUriAsync(uri);
        }
    }
}
