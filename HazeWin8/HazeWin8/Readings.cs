﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media;

namespace HazeWin8
{
    public class City
    {
        public String State { get; set; }
        public String ID { get; set; }
        public Int32 API { get; set; }
        public SolidColorBrush Seriousness { get; set; }
        public String Location { get; set; }
        public String Updated { get; set; }
    }

    public class State
    {
        public String StateName { get; set; }
        public ObservableCollection<City> Cities { get; set; }
    }
}
