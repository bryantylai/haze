﻿#define DEBUG_AGENT
using System.Windows;
using Microsoft.Phone.Scheduler;
using Microsoft.Phone.Shell;

using System.Collections.Generic;
using System;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.IO;
using System.Net.NetworkInformation;
using System.IO.IsolatedStorage;

namespace LiveTileScheduledTaskAgent
{
    public class ScheduledAgent : ScheduledTaskAgent
    {
        private static volatile bool _classInitialized;
        ScheduledTask scheduledTask;

        /// <remarks>
        /// ScheduledAgent constructor, initializes the UnhandledException handler
        /// </remarks>
        public ScheduledAgent()
        {
            if (!_classInitialized)
            {
                _classInitialized = true;
                // Subscribe to the managed exception handler
                Deployment.Current.Dispatcher.BeginInvoke(delegate
                {
                    Application.Current.UnhandledException += ScheduledAgent_UnhandledException;
                });
            }
        }

        /// Code to execute on Unhandled Exceptions
        private void ScheduledAgent_UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                // An unhandled exception has occurred; break into the debugger
                System.Diagnostics.Debugger.Break();
            }
        }
        /// <summary>
        /// Agent that runs a scheduled task
        /// </summary>
        /// <param name="task">
        /// The invoked task
        /// </param>
        /// <remarks>
        /// This method is called when a periodic or resource intensive task is invoked
        /// </remarks>
        protected override void OnInvoke(ScheduledTask task)
        {
            scheduledTask = task;
            try
            {
                using (StreamReader readFile = new StreamReader(new IsolatedStorageFileStream("default.txt", FileMode.Open, FileAccess.Read, IsolatedStorageFile.GetUserStoreForApplication())))
                {
                    GetResults(readFile.ReadLine());
                }
            }
            catch
            {
                GetResults("http://apps.evozi.com/myapi/?loc=petaling_jaya");
            }
        }

        private void UpdateTile(String api, String psinowdate)
        {
            // get application tile
            ShellTile tile = ShellTile.ActiveTiles.First();
            if (null != tile)
            {
                // creata a new data for tile
                StandardTileData data = new StandardTileData();
                // tile foreground data
                data.Title = "MY Haze";
                data.BackgroundImage = new Uri("/Background.png", UriKind.Relative);
                // to make tile flip add data to background also
                data.BackTitle = "MY Haze";
                data.BackContent = "Current API : " + api + "\nLast update " + psinowdate;
                // update tile
                tile.Update(data);
            }

#if(DEBUG_AGENT)
            ScheduledActionService.LaunchForTest(scheduledTask.Name, TimeSpan.FromMinutes(15));
            System.Diagnostics.Debug.WriteLine("Periodic task is started: " + scheduledTask.Name);
#endif
            NotifyComplete();
        }

        private void GetResults(String uri)
        {
            if (NetworkInterface.GetIsNetworkAvailable())
            {
                HttpWebRequest req = HttpWebRequest.CreateHttp(uri);
                req.BeginGetResponse(new AsyncCallback(ReadWebRequestCallBack), req);
            }
            else
            {
            }
        }

        private void ReadWebRequestCallBack(IAsyncResult result)
        {
            try
            {
                HttpWebRequest req = (HttpWebRequest)result.AsyncState;
                HttpWebResponse res = (HttpWebResponse)req.EndGetResponse(result);

                using (StreamReader httpwebStreamReader = new StreamReader(res.GetResponseStream()))
                {
                    String results = httpwebStreamReader.ReadToEnd();

                    MatchCollection psinow = Regex.Matches(results, "<div class=\"psinow\">.*?</div>", RegexOptions.Singleline);
                    MatchCollection psinowdate = Regex.Matches(results, "<div class=\"psinowdate\">.*?</div>", RegexOptions.Singleline);

                    String api = Regex.Replace(psinow[0].Value, @"<[^<]*>", String.Empty);
                    String update = Regex.Replace(psinowdate[0].Value, @"<[^<]*>", String.Empty);

                    UpdateTile(api, update);
                }
            }
            catch
            {
            }
        }
    }
}